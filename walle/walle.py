import cv2
import numpy as np
import time
from mobilenet import MobileNet
from background_motion import BackgroundMotionDetector

from ip_camera_streamer.ipcamera import IPCamera
from ip_camera_streamer.streamer import Streamer
from ip_camera_streamer.webcamstreamer import WebCamStreamer

class Walle:

    def __init__(self, classIds, delegate, imageProcessor, backgroundMotionMinArea=8000, maxFrameSpace=10, ipCamera=None):
        self.classIds = classIds
        self.delegate = delegate
        self.maxFrameSpace = maxFrameSpace
        self.frameCounter = 0
        self.ipCamera = ipCamera
        self.imageProcessor = imageProcessor
        self.backgroundMotionDetector = BackgroundMotionDetector(min_area=backgroundMotionMinArea)

        if ipCamera is None:
            self.streamer = WebCamStreamer()
        else:
            self.streamer = Streamer(self.ipCamera)

        #self.streamer = Streamer(self, IPCamera(url="http://192.168.0.34", port=5000, username=None, password=None, videoFolder="video_feed", videoName=None, videoParams=None))
        #self.streamer = IPCameraStreamer(self, Camera(url="http://67.53.223.42", port=80, username=None, password=None, videoFolder="mjpg", videoName="video.mjpg", videoParams=None))
        #self.streamer = IPCameraStreamer(self, Camera(url="http://153.220.104.229", port=81, username=None, password=None, videoFolder=None, videoName="SnapshotJPEG", videoParams="Resolution=640x480&Quality=Clarity&1495763819"))
        #self.motionDetector = MotionDetection(self, delta=5, min_area=5000)
        #self.recognizer = ImPrediction()

    def start(self):
        self.streamer.startStreaming()

        while True:
            image = self.streamer.read()
            if image is not None:

                self.backgroundMotionDetector.processBg(image)

                if self.frameCounter == self.maxFrameSpace:
                    self.frameCounter = 0

                    detections = self.imageProcessor.process(image)

                    for i in np.arange(0, detections.shape[2]):
                        confidence = detections[0, 0, i, 2]
                        if confidence > 0.5:
                            idx = int(detections[0, 0, i, 1])
                            (h, w) = image.shape[:2]
                            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                            (startX, startY, endX, endY) = box.astype("int")
                            label = "{}: {:.2f}%".format(self.imageProcessor.CLASSES[idx],
                                confidence * 100)
                            cv2.rectangle(image, (startX, startY), (endX, endY),
                                self.imageProcessor.COLORS[idx], 2)
                            y = startY - 15 if startY - 15 > 15 else startY + 15
                            cv2.putText(image, label, (startX, y),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.5, self.imageProcessor.COLORS[idx], 2)

                            if self.imageProcessor.CLASSES[idx] == "person":
                                self.delegate.identify_object_with_class_id(idx, image)
                else:
                    self.frameCounter += 1

                self.showVideo(image)

    def stop(self):
        #self.streamer.stopSteaming()
        cv2.destroyAllWindows()

    def showVideo(self, jpg):
        cv2.imshow('Mobile IP Camera', jpg)
        # Exit key
        if cv2.waitKey(1) & 0xFF == ord('q'):
            self.stop()
            exit(0)
