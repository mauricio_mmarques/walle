import numpy as np
import imutils
import cv2

class BackgroundMotionDetector:

    def __init__(self, min_area):
        self.background_frame = None
        self.min_area = min_area

    def processBg(self, image):
        # resize the frame, convert it to grayscale, and blur it
        frame = imutils.resize(image, width=500)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (21, 21), 0)

        if self.background_frame is None:
            self.background_frame = gray.copy().astype("float")
            return

        # compute the absolute difference between the current frame and
        # first frame
        cv2.accumulateWeighted(gray, self.background_frame, 0.5)
        frameDelta = cv2.absdiff(gray, cv2.convertScaleAbs(self.background_frame))
        thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]

        # dilate the thresholded image to fill in holes, then find contours
        # on thresholded image
        thresh = cv2.dilate(thresh, None, iterations=2)
        _, cnts, _ = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        # loop over the contours
        for c in cnts:
            # if the contour is too small, ignore it
            if cv2.contourArea(c) < self.min_area:
                continue

            print "Changed"
