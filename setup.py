from setuptools import setup

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(
    name='walle',
    version='0.1',
    author="Mauricio Martinez Marques",
    package_data={'': ['*.pb', '*.pbtxt','*.caffemodel','*.prototxt.txt']},
    include_package_data=True,
    packages=['walle'],
    platforms=['any'],
    install_requires=required
)
